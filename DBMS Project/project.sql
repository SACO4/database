DROP DATABASE `lol`;
CREATE SCHEMA `lol`;

# Creating Tables --------------------------------------
create table lol.Adminstrator (admin_id int primary key, admin_age int, admin_email varchar(30) unique, admin_name varchar(20), admin_dob DATE, store_id int);
create table lol.Cart (cart_id int primary key, amount int, item_qty int);
create table lol.Item (item_id int primary key, item_price int, item_name varchar(30), item_qty int);
create table lol.Delivery_boy (db_id int, db_name varchar(30), c_locality varchar(20));
create table lol.Store (store_id int primary key, store_name varchar(20) , store_city varchar(30));
create table lol.Customer (c_id int primary key, c_name varchar(30), cart_id int, c_locality varchar(100), c_city varchar(30));
create table lol.DelCart (cart_id int, db_id int);
create table lol.StoreDB (store_id int, db_id int);
create table lol.StoreIt (store_id int, item_id int);
create table lol.CartIt (cart_id int, item_id int);
alter table lol.Customer add index c_address (c_locality, c_city);

# Inserting rows into relations ------------------------

# Entries in Administrator table
insert into lol.Adminstrator values (001, 19, 'sachin17361@iitd.ac.in', 'Sachin', "1999-08-04", 001);
insert into lol.Adminstrator values (002, 19, 'nakul17068@iitd.ac.in', 'Nakul', "1999-08-04", 002);
insert into lol.Adminstrator values (003, 20, 'durvish17231@iitd.ac.in', 'Durvish', "1999-03-18", 003);
insert into lol.Adminstrator values (004, 19, 'shiva17190@iitd.ac.in', 'Shiva', "1999-06-18", 004);
insert into lol.Adminstrator values (005, 20, 'utsav17269@iitd.ac.in', 'Utsav', "1999-01-08", 005);
# Show all the entries in Administrator Table
#select * from lol.Adminstrator;

# Entries in Cart table
insert into lol.Cart values (001, 10000, 1), (002, 94000, 2), (003, 120000, 2), (004, 65000, 2), (005, 103000, 3);
insert into lol.Cart values (006, 130000, 2), (007, 50000, 1), (008, 94000, 2), (009, 100000, 1), (010, 24000, 2);
# Show all the entries in Cart Table
#select * from lol.Cart;

# Entries in Item table
insert into lol.Item values (001, 20000, 'Washing Machine', 10), (002, 10000, 'Microwave', 5), (003, 15000, 'MI A2 Lite', 30);
insert into lol.Item values (004, 34000, 'Sony BRAVIA 40-inch', 6), (005, 60000, 'Macbook Air', 9), (006, 4000, 'Logitech G502', 10);
insert into lol.Item values (007, 100000, 'HP Omen Gaming Laptop', 50), (008, 80000, 'Macbook Pro', 70), (009, 8000, 'Acoustic Guitar', 10);
insert into lol.Item values (010, 50000, 'Sofa Set', 7);
# Show all the entries in Item Table
#select * from lol.Item;

# Entries in Store table
insert into lol.Store values(001,'Amacon','Gurgaon');
insert into lol.Store values(002,'Supermarche','Noida');
insert into lol.Store values(003,'7 Eleven','Gurgaon');
insert into lol.Store values(004,'Walmart','Delhi');
insert into lol.Store values(005,'24 Seven','Delhi');
# Show all the entries in Store Table
#select * from lol.Store;

# Entries in Delivery_boy table
insert into lol.Delivery_boy values (001, 'Apratim Ankit', 'Okhla'), (002, 'Rachit Jain', 'IIITD');
insert into lol.Delivery_boy values (002, 'Rachit Jain','Govindpuri'), (001, 'Apratim Ankit', 'South Ex');
insert into lol.Delivery_boy values (003, 'Shiva Saini', 'Dwarka Mor'), (004, 'Utsav Gangwar', 'Rajouri Gardden');
insert into lol.Delivery_boy values (005, 'Bhavye Gupta', 'Rohini'), (006, 'Mayank Jain', 'Dilshad Garden');
# Show all the entries in Delivery_boy Table
#select * from lol.Delivery_boy;

# Entries in Customer table
insert into lol.Customer values (001, 'Nakul', 001, 'Okhla', 'Delhi'), (002, 'Durvish', 005, 'South Ex', 'Delhi');
insert into lol.Customer values (004, 'Utsav', 009, 'IIITD', 'Gurgaon'), (006, 'Shivam', 008, 'Okhla', 'Delhi');
insert into lol.Customer values (007, 'Akash', 006, 'Govindpuri', 'Delhi'), (009, 'Ankit', 002, 'Rajouri Garden', 'Delhi');
insert into lol.Customer values (010, 'Abhishek', 004, 'South Ex', 'Noida'), (003, 'Shiva', 007, 'Dwarka Mor', 'Gurgaon');
insert into lol.Customer values (008, 'Akhil', 010, 'Dilshad Garden', 'Noida'), (005, 'Prey', 003, 'Rohini', 'Delhi');
# Show all the entries in Customer Table
#select * from lol.Customer;

# Entries in DelCart table
insert into lol.DelCart values (001, 001), (002, 002), (003, 002), (004, 003), (005, 001), (006, 004), (007, 005);
# Show all the entries in DelCart Table
#select * from lol.DelCart;

# Entries in StoreDB table
insert into lol.StoreDB values (001, 003), (001, 005), (002, 001), (003, 002), (004, 006), (005, 004);
# Show all the entries in StoreDB Table
#select * from lol.StoreDB;

# Entries in StoreIt table
insert into lol.StoreIt values (001, 001), (001, 009), (001, 010), (002, 001), (002, 003), (002, 004), (003, 004);
insert into lol.StoreIt values (003, 007), (003, 009), (004, 002), (004, 005), (005, 006), (005, 007), (005, 008);
# Show all the entries in StoreIt Table
#select * from lol.StoreIt;

# Entries in CartIt table
insert into lol.CartIt values (001, 002), (002, 004), (002, 005), (003, 007), (003, 001), (004, 010), (004, 003);
insert into lol.CartIt values (005, 008), (005, 003), (005, 009), (006, 010), (006, 008), (007, 010), (008, 004);
insert into lol.CartIt values (008, 005), (009, 007), (010, 006), (010, 001);
# Show all the entries in CartIt Table
#select * from lol.CartIt;

#Select Queries
#select * from lol.Adminstrator;
#select * from lol.Cart;
#select * from lol.Item;
#select * from lol.Store;
#select * from lol.Delivery_boy;
#select * from lol.DelCart;
#select * from lol.StoreDB;
#select * from lol.StoreIt;
#select * from lol.CartIt;

# Conditional Search Query
#select * from lol.Adminstrator where admin_name = 'sachin';
#select c_name from lol.Customer where cart_id = 001;

# Update Query
#update lol.Delivery_boy set c_locality='Govindpuri' where c_locality='Okhla';
#select * from lol.Delivery_boy;

# Delete Query
#delete from lol.Item where item_id= 009;
#select * from lol.Item;

# Groupby Query
#select db_id from lol.Delivery_boy group by db_id;

# Count Query
#select db_id, count(*) from lol.Delivery_boy group by db_id;

# Natural Join Query
#select * from lol.Adminstrator natural join lol.Store;

# Sorting by Attribute Query
#select * from lol.Delivery_boy order by db_id;

# Conditional Cross Product Query
#select * from lol.DelCart as t1, lol.Cart as t2 where t1.db_id = 001 and t1.cart_id = t2.cart_id;
